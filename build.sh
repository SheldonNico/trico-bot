#! /usr/bin/env bash
#docker run --name trico -it -v /home/xnie/projects/trico-bot:/trico-bot clux/muslrust
docker run \
  -v cargo-cache:/root/.cargo/registry \
  -v "$PWD:/volume" \
  --rm -it clux/muslrust cargo build --release && \
  rsync -avz ./target/x86_64-unknown-linux-musl/release/trico-bot vultr:/root/bin # && ssh vultr 'cd /root/bin; ./trico-bot'
