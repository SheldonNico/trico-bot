use chrono::Weekday;
use lettre::smtp::authentication::Credentials;
use num_traits::cast::FromPrimitive;
use serde::{self, de::{self, Visitor, Deserializer}, Serialize, Deserialize};
use std::collections::{HashMap, HashSet};
use std::fmt;
use lazy_static::lazy_static;

#[derive(Debug, Clone, Deserialize, Eq, PartialEq)]
pub struct TricoConfig {
    pub ssh: HashMap<String, SSHConfig>,
    pub tail: HashMap<String, TailConfig>,
    pub basic: BasicConfig,

    pub telegram: Option<TelegramConfig>,
    pub email: Option<EmailConfig>,
    pub tencent: Option<TencentConfig>,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq, Eq)]
pub struct BasicConfig {
    #[serde(default)]
    pub standby: bool,
    #[serde(default = "serde_funs::default_max_alter_count")]
    pub max_alter_count: u32,
    #[serde(default = "serde_funs::default_alter_reset_min")]
    pub alter_reset_min: u32,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq, Eq)]
pub struct SSHConfig {
    pub address: String,
    pub username: String,

    #[serde(default)]
    pub password: String,
    #[serde(default)]
    pub privatekey: String,
    #[serde(default)]
    pub pubkey: Option<String>,
    #[serde(default)]
    pub passphrase: Option<String>,
    #[serde(default)]
    pub max_connection: u8,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Regex(
    #[serde(deserialize_with = "serde_funs::de_regex")]
    pub regex::Regex
);

#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct TailConfig {
    #[serde(default = "serde_funs::default_source")]
    pub source: String,

    pub path: String,
    #[serde(default)]
    pub ignored_regex: Vec<Regex>,
    #[serde(default = "serde_funs::default_vec_ts")]
    pub time_span: Vec<TimeSpan>,
    #[serde(default = "serde_funs::default_outdated_min")]
    pub outdated_min: i32,
    #[serde(default = "serde_funs::default_offset_n")]
    pub offset_n: i32,

    #[serde(default)]
    pub is_disable: bool,
    #[serde(default)]
    pub is_not_found_alter: bool,
    #[serde(default)]
    pub is_follow: bool,

    // if we want parse log
    #[serde(default)]
    pub log_regex: Option<Regex>,
    #[serde(default)]
    pub log_level_map: HashMap<String, String>,
    #[serde(default = "serde_funs::default_alter_level", deserialize_with = "serde_funs::de_loglevel")]
    pub log_level_alter: log::Level,
    #[serde(default = "serde_funs::default_notice_level", deserialize_with = "serde_funs::de_loglevel")]
    pub log_level_notice: log::Level,
}

#[derive(Debug, Clone, Deserialize, Eq, PartialEq)]
pub struct TelegramConfig {
    #[serde(default)]
    pub token: String,
    #[serde(default)]
    pub chat: Option<telegram_bot::types::Chat>,
}

#[derive(Debug, Clone, Deserialize, Eq, PartialEq)]
pub struct EmailConfig {
    pub auth: Credentials,
    pub addr: String,
    pub sender: String,
    pub receivers: Vec<String>,
}

#[derive(Debug, Clone, Deserialize, Eq, PartialEq)]
pub struct TencentConfig {
    pub base_url: String,
    pub app_key: String,
    pub app_id: u64,
    pub receivers: Vec<String>
}

type MinutePrecision = (u32, u32);
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct TimeSpan {
    st:       MinutePrecision,
    ed:       MinutePrecision,
    weekdays: HashSet<Weekday>,
}

impl Default for TimeSpan {
    fn default() -> Self {
        Self {
            st: (0, 0), ed: (23, 59), weekdays: (0..7).filter_map(Weekday::from_i64).collect()
        }
    }
}

impl TimeSpan {
    pub fn is_in_span<T: chrono::Timelike + chrono::Datelike>(&self, dt: &T) -> bool {
        if self.weekdays.contains(&dt.weekday()) {
            let dt_min = (dt.hour(), dt.minute());
            if self.st < self.ed && self.st <= dt_min && dt_min <= self.ed {
                return true;
            } else if self.st > self.ed && (self.st <= dt_min || dt_min <= self.ed) {
                return true;
            }
        }
        false
    }
}

mod serde_funs {
    use super::*;

    pub fn de_regex<'de, D>(deserializer: D) -> Result<regex::Regex, D::Error>
    where
        D: de::Deserializer<'de>,
    {
        struct Visitor;

        impl<'de> de::Visitor<'de> for Visitor {
            type Value = regex::Regex;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("a string which is valid regex string")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: de::Error,
            {
                regex::Regex::new(v).map_err(E::custom)
            }
        }

        deserializer.deserialize_str(Visitor)
    }

    pub fn de_loglevel<'de, D>(deserializer: D) -> Result<log::Level, D::Error>
    where
        D: de::Deserializer<'de>,
    {
        struct Visitor;

        impl<'de> de::Visitor<'de> for Visitor {
            type Value = log::Level;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("string not a valid loglevel")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: de::Error,
            {
                v.parse().map_err(E::custom)
            }
        }

        deserializer.deserialize_str(Visitor)
    }

    pub fn default_notice_level() -> log::Level {
        log::Level::Info
    }

    pub fn default_alter_level() -> log::Level {
        log::Level::Error
    }

    pub fn default_outdated_min() -> i32 {
        3
    }

    pub fn default_alter_reset_min() -> u32 {
        60
    }

    pub fn default_max_alter_count() -> u32 {
        3
    }

    pub fn default_vec_ts() -> Vec<TimeSpan> {
        vec![TimeSpan::default()]
    }

    pub fn default_source() -> String {
        "-".into()
    }

    pub fn default_offset_n() -> i32 {
        -1
    }
}

struct TimeSpanVisitor;
impl<'de> Visitor<'de> for TimeSpanVisitor {
    type Value = TimeSpan;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "a valid timespan string.")
    }

    fn visit_str<E>(self, raw: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        static PATSTR: &'static str = r"(?P<wd>\d\-\d:|\d:)?(?P<st>\d{2}:\d{2})-(?P<ed>\d{2}:\d{2})";
        lazy_static! {
            static ref PAT: regex::Regex = regex::Regex::new(PATSTR)
            .unwrap();
        }

        let res = {
            match PAT.captures(raw) {
                None => return Err(E::custom(format!("{} not match regex format: {}", raw, PATSTR))),
                Some(caps) => caps,
            }
        };

        let weekdays: HashSet<Weekday> = match res.name("wd") {
            None => (0..7)
                .into_iter()
                .map(|n| Weekday::from_i64(n).unwrap())
                .collect(),
            Some(val) => {
                let val = val.as_str();
                let vals: Vec<&str> = val[..val.len() - 1].split('-').collect(); // reove last ':'
                if vals.len() == 1 {
                    let st = std::cmp::max(0, std::cmp::min(vals[0].parse().map_err(de::Error::custom)?, 6i64));
                    (st..st + 1)
                        .into_iter()
                        .map(|n| Weekday::from_i64(n).unwrap())
                        .collect()
                } else if vals.len() == 2 {
                    let st = std::cmp::max(0, std::cmp::min(vals[0].parse().map_err(de::Error::custom)?, 6i64));
                    let ed = std::cmp::max(0, std::cmp::min(vals[1].parse().map_err(de::Error::custom)?, 6i64));
                    (st..ed + 1)
                        .into_iter()
                        .map(|n| Weekday::from_i64(n).unwrap())
                        .collect()
                } else {
                    unreachable!()
                }
            }
        };

        let st: MinutePrecision = match res.name("st") {
            Some(mat) => {
                let vals: Vec<&str> = mat.as_str().split(":").collect();
                (vals[0].parse().map_err(de::Error::custom)?, vals[1].parse().map_err(de::Error::custom)?)
            }
            _ => unreachable!(),
        };
        let ed: MinutePrecision = match res.name("ed") {
            Some(mat) => {
                let vals: Vec<&str> = mat.as_str().split(":").collect();
                (vals[0].parse().map_err(de::Error::custom)?, vals[1].parse().map_err(de::Error::custom)?)
            }
            _ => unreachable!(),
        };

        Ok(TimeSpan { st, ed, weekdays })
    }
}
impl<'de> Deserialize<'de> for TimeSpan {
    fn deserialize<D>(deserializer: D) -> Result<TimeSpan, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(TimeSpanVisitor)
    }
}

impl PartialEq for Regex {
    fn eq(&self, other: &Self) -> bool {
        self.0.as_str() == other.0.as_str()
    }
}
impl Eq for Regex {  }

impl Default for TailConfig {
    fn default() -> Self {
        TailConfig {
            source:             serde_funs::default_source(),
            path:               Default::default(),
            ignored_regex:      Default::default(),
            time_span:          serde_funs::default_vec_ts(),
            outdated_min:       serde_funs::default_outdated_min(),
            offset_n:           serde_funs::default_offset_n(),
            is_disable:         Default::default(),
            is_not_found_alter: Default::default(),
            is_follow:          Default::default(),
            log_regex:          Default::default(),
            log_level_map:      Default::default(),
            log_level_alter:    serde_funs::default_alter_level(),
            log_level_notice:   serde_funs::default_notice_level(),
        }
    }
}
