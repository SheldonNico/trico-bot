use crate::logtail::{*, Record};
use crate::configs::*;
use crate::telegram::start_bot;
use crate::util::{modified_time, map_diff, sha256_digest};

use tokio::runtime;
use telegram_bot::{Api, CanSendMessage};
use chrono::{DateTime, Local, TimeZone, Weekday};
use lettre::smtp::{authentication::{Credentials, Mechanism}, ConnectionReuseParameters};
use lettre::{SendableEmail, Envelope, EmailAddress, Transport, SmtpClient};
use lettre_email::Email;
use num_traits::cast::FromPrimitive;
use ssh2::{Session, Sftp};
use std::collections::{HashMap, HashSet};
use std::sync::{Arc, Mutex};
use std::time::SystemTime;
use lazy_static::lazy_static;
use serde::{Serialize, Deserialize};
use rayon::prelude::*;
use log::*;
use crossbeam::channel::{self, Sender, Receiver, select};
use serde_json::json;

pub struct TricoState {
    pub log_alter_last: SystemTime,
    pub log_alter_count: u32,
    pub log_alter_standby: bool,

    pub email_idx: usize,
    pub phone_idx: usize,

    pub tx: Vec<(Sender<Record>, bool)>,
    pub trico_config: TricoConfig
}

pub struct TricoLog {
    trico_config: TricoConfig,

    connections: HashMap<String, Vec<Connection>>,
    tails: HashMap<String, FileSpec>,

    pub state: Arc<Mutex<TricoState>>,
}

/// sftp and session are boundled, tow sftp read in the same session will influence each other
pub enum Connection {
    SSH { session: Session, sftp: Sftp, last_refresh: std::time::Instant },
    Local,
}

pub enum ConnectionRef<'c> {
    Sftp(&'c Sftp),
    // Ftp(&'C Ftp),
    Local,
}

pub struct Notifier {
    pub state: Arc<Mutex<TricoState>>,
    pub rx_msg: Receiver<Record>,
}

impl Notifier {
    pub fn new(state: Arc<Mutex<TricoState>>) -> (Self, Sender<Record>) {
        let (tx_msg, rx_msg) = channel::bounded(1024);
        ( Self { state, rx_msg }, tx_msg )
    }

    pub async fn run_telegram(&self) {
        debug!("telegram notifier started ...");
        let token; let chat;
        {
            let state = & *self.state.lock().unwrap();
            match &state.trico_config.telegram {
                Some(c) => {
                    token = c.token.clone();
                    chat = c.chat.clone();
                },
                _ => {
                    token = "".into(); chat = None;
                }
            }
        };

        if token.len() > 0 {
            let api =  Api::new(token);
            tokio::spawn(start_bot(self.state.clone(), api.clone(), chat.clone().expect("start_bot with empty chat")));

            let mut check_msg_now = std::time::Instant::now(); let mut msg_interval = 250;
            let mut stack = vec![];
            loop {
                let mut is_received = false;
                let is_stack_filled = stack.len() > 0;
                while let Ok(record) = self.rx_msg.try_recv() {
                    is_received = true;
                    stack.push(record);
                }

                if stack.len() > 0 && check_msg_now.elapsed() > std::time::Duration::from_millis(msg_interval) {
                    msg_interval = if is_stack_filled && is_received {
                        debug!("got more message, scale up msg_interval: {}", msg_interval);
                        (2 * msg_interval).min(1000 * 60)
                    } else {
                        debug!("got less message, scale down msg_interval: {}", msg_interval);
                        (msg_interval / 2).max(250)
                    };
                    check_msg_now = std::time::Instant::now();

                    let msg = Self::format_telegram_msg(&stack); stack.clear();

                    let reply = chat.as_ref().unwrap().text(msg);
                    debug!("send log message: {:?}", reply);
                    if let Err(e) = api.send(reply).await { warn!("fail to send msg: {}.", e); }
                }

                tokio::time::delay_for(std::time::Duration::from_millis(100)).await;
            }
        } else {
            loop {
                while let Ok(msg) = self.rx_msg.try_recv() {
                    trace!("sending to notify telegram {:?}", msg);
                }

                tokio::time::delay_for(std::time::Duration::from_millis(100)).await;
            }
        }
    }

    pub fn format_telegram_msg(stack: &Vec<Record>) -> String {
        let mut msgs = vec![];
        for record in stack.iter() {
            let msg_limt_len = &record.msg[..100.min(record.msg.len())];
            if record.source == "" {
                // all msg from "" is from our system. we want it be the same.
                msgs.push(format!("{} {}", record.level, record.msg));
            } else {
                if record.is_alter {
                    msgs.push(format!("[A] [{} {}] {}", record.source, record.level, msg_limt_len));
                } else {
                    msgs.push(format!("[N] [{} {}] {}", record.source, record.level, msg_limt_len));
                }
            }
        }

        msgs.sort();
        let mut content: String = msgs[..10.min(msgs.len())].join("\n");
        if msgs.len() > 10 {
            content = format!("{}\nand more({})...", content, msgs.len()-10);
        }

        content
    }

    pub async fn run_email(&self) {
        debug!("email notifier started ...");
        loop {
            while let Ok(msg) = self.rx_msg.try_recv() {
                if !msg.is_alter { continue; }
                trace!("sending to notify email {:?}", msg);
                let addr; let auth; let sender; let receiver;
                {
                    let state = &mut *self.state.lock().unwrap();
                    if let Some(ref config) = state.trico_config.email {
                        auth = config.auth.clone();
                        addr = config.addr.clone();
                        sender = config.sender.clone();
                        if config.receivers.len() > 0 {
                            receiver = config.receivers[state.email_idx % config.receivers.len()].clone();
                            state.email_idx += 1;
                        } else {
                            receiver = "".into();
                        }
                    } else {
                        continue;
                    }
                }

                if addr.len() > 0 && sender.len() > 0 && receiver.len() > 0 {
                    if let Ok(mail) = Email::builder()
                        .subject("Trico: alter detect in log file")
                            .html(format!("{:?}", msg))
                            .from(sender)
                            .to(receiver.clone())
                            .build()
                    {
                        if let Err(e) = send_email(mail, &addr, auth).await {
                            error!("fail send email alter to {}", e);
                        } else {
                            debug!("Successfully send email alter to {:?}", receiver);
                        }
                    }
                }
            }

            tokio::time::delay_for(std::time::Duration::from_millis(100)).await;
        }
    }

    pub async fn run_tencent(&self) {
        debug!("tencent notifier started ...");
        loop {
            // NOTE: try_recv prevent blocking
            while let Ok(msg) = self.rx_msg.try_recv() {
                if !msg.is_alter { continue; }
                trace!("sending to notify tencent {:?}", msg);
                // let mut base_url = "".into(); let mut app_key = "".into(); let mut receivers = vec![]; let mut app_id = 0;
                let base_url; let app_key; let receiver; let app_id;
                {
                    let state = &mut *self.state.lock().unwrap();
                    if let Some(ref config) = state.trico_config.tencent {
                        base_url  = config.base_url.clone();
                        app_key   = config.app_key.clone();
                        app_id    = config.app_id;

                        if config.receivers.len() > 0 {
                            receiver = config.receivers[state.phone_idx % config.receivers.len()].clone();
                            state.phone_idx += 1;
                        } else {
                            receiver = "".into();
                        }
                    } else {
                        continue;
                    };
                };

                if base_url.len() > 0 && app_key.len() > 0 && app_id > 0 && receiver.len() > 0 {
                    if let Err(e) = send_sms_tencent(receiver.clone(), &base_url, &app_key, app_id).await {
                        error!("fail send phone alter to {}", e);
                    } else {
                        debug!("Successfully send phone alter to {:?}", receiver);
                    }
                }
            }

            tokio::time::delay_for(std::time::Duration::from_millis(100)).await;
        }
    }
}

pub async fn send_email(mail: Email, addr: &str, auth: Credentials) -> Result<(), Box<dyn std::error::Error>> {
    trace!("Starting sending email: {:?}", mail);
    // Connect to a remote server on a custom port
    let client = SmtpClient::new_simple(addr)?;

    let mut mailer = client
        .credentials(auth)
        .smtp_utf8(false)
        .authentication_mechanism(Mechanism::Plain)
        .transport();

    if cfg!(debug_assertions) {
        info!("Faked email api for debug");
    } else {
        mailer.send(mail.into())?;
        mailer.close();
    }

    Ok(())
}

pub async fn send_sms_tencent(mobile: String, base_url: &str, app_key: &str, app_id: u64) -> Result<(), Box<dyn std::error::Error>> {
    use rand::Rng;
    let client = reqwest::Client::new();
    let random: u32 = rand::thread_rng().gen();
    let time = std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap().as_secs();

    let secrete_str = format!("appkey={appkey}&random={random}&time={time}&mobile={mobile}",
        appkey=app_key, random=random, time=time, mobile=mobile);
    let sig = sha256_digest(secrete_str.as_bytes()).unwrap();
    debug!("calulate signature for tele msg: {} -> {}", secrete_str, sig);

    let body = json!({
        "tpl_id": 296291,
        "params": [ "服务器" ],
        "playtimes": 1,
        "sig": sig,
        "tel": {
            "mobile": mobile,
            "nationcode": "86"
        },
        "time": time,
        "ext": ""
    });

    if cfg!(debug_assertions) {
        info!("Faked tencent api for debug");
    } else {
        let res = client.post(&format!("{}?sdkappid={}&random={}", base_url, app_id, random))
            .json(&body).send().await?;

        debug!("Successfully send post request to tencent api {}, res: {}",
            mobile,
            res.text().await.unwrap_or("Fail to decode response".into()));
    }

    Ok(())
}

pub async fn run_config_update(state: Arc<Mutex<TricoState>>, fname: String) {
    debug!("update config starting...");
    let mut modified = modified_time(fname.as_ref())
        .expect("Fail to get file modified time at startup");

    loop {
        let latest = modified_time(fname.as_ref())
            .expect("Fail to get file modified time at startup");
        if latest != modified {
            match TricoState::read_config(&fname) {
                Ok(trico_config) => {
                    let state = &mut *state.lock().unwrap();
                    let _oconfig = state.update_config(trico_config);
                    let msg = format!("config file updated, load latest config: {}",
                        latest.format("%Y-%m-%d %H:%M:%S"));
                    info!("{}", msg); state.notify(msg);

                    /*
                     * if oconfig.basic.standby && !state.trico_config.basic.standby {
                     *     // true -> false
                     *     let msg = "config file updated, standy off: true -> false";
                     *     info!("{}", msg); state.notify(msg);
                     *     state.reset_standby();
                     * } else if !oconfig.basic.standby && state.trico_config.basic.standby {
                     *     // false -> true
                     *     let msg = "config file updated, standy on: false -> true";
                     *     info!("{}", msg); state.notify(msg);
                     *     state.set_standby();
                     * }
                     */
                },
                Err(e) => {
                    error!("updating config failed: {}", e);
                }
            }

            modified = latest;
        }

        tokio::time::delay_for(std::time::Duration::from_millis(100)).await;
    }
}

impl TricoState {
    pub fn read_config(fname: &str) -> Result<TricoConfig, Box<dyn std::error::Error>> {
        let mut cf = config::Config::new();
        cf
            .merge(config::File::with_name(fname))?
            .merge(config::Environment::with_prefix("Trico"))?;
        Ok(cf.try_into()?)
    }

    pub fn from_file(fname: &str) -> Result<Self, Box<dyn std::error::Error>> {
        let slf = Self::from_config(Self::read_config(fname)?);
        Ok(slf)
    }

    pub fn from_config(trico_config: TricoConfig) -> Self {
        Self {
            log_alter_last: SystemTime::now(),
            log_alter_count: 0,
            log_alter_standby: trico_config.basic.standby,

            email_idx: 0,
            phone_idx: 0,

            tx: vec![],
            trico_config
        }
    }

    fn update_config(&mut self, trico_config: TricoConfig) -> TricoConfig {
        if !self.log_alter_standby && trico_config.basic.standby {
            self.set_standby();
            let msg = "set standby on by config"; info!("{}", msg); self.notify(msg);
        } else if self.log_alter_standby && !trico_config.basic.standby {
            self.reset_standby();
            let msg = "set standy off by config"; info!("{}", msg); self.notify(msg);
        }

        std::mem::replace(&mut self.trico_config, trico_config)
    }

    pub fn reset_standby(&mut self) {
        self.log_alter_count = 0;
        self.log_alter_standby = false;
    }

    pub fn set_standby(&mut self) {
        self.log_alter_standby = true;
        self.log_alter_count = 0;
        self.log_alter_last = SystemTime::now();
    }

    fn bind_notify(&mut self, tx: Sender<Record>, is_alter: bool) -> &mut Self {
        self.tx.push((tx, is_alter));
        self
    }

    fn notify<S: AsRef<str>>(&self, msg: S) {
        for (tx, is_alter) in self.tx.iter() {
            if !is_alter {
                tx.send(Record::info(msg.as_ref())).ok();
            }
        }
    }

    fn alter<S: AsRef<str>>(&self, msg: S) {
        for (tx, _is_alter) in self.tx.iter() {
            tx.send(Record::alter(msg.as_ref())).ok();
        }
    }

    // 如何判定是否该alter
    // 1. 不处于standby && alter 累计少于配置最大alter
    // 2. 处于 standy， 但上一次进入standby的时间距今查过了最大stanby_min
    pub fn prepare_alter(&mut self) -> bool {
        if self.log_alter_standby &&
            (self.log_alter_last.elapsed().unwrap().as_secs() / 60 > self.trico_config.basic.alter_reset_min as u64)
        {
            let last_alter_dt: DateTime<Local> = self.log_alter_last.into();
            let msg = format!("alter standby timeout, reset automatically {}", last_alter_dt.format("%Y%m%d %H:%M"));
            info!("{}", msg); self.notify(msg);
            self.reset_standby();
        }

        !self.log_alter_standby && self.log_alter_count < self.trico_config.basic.max_alter_count
    }

    pub fn finish_alter(&mut self) {
        self.log_alter_count += 1;
        if self.log_alter_count >= self.trico_config.basic.max_alter_count {
            let msg = format!("alter more than {} times, set standy to true", self.log_alter_count);
            info!("{}", msg); self.notify(msg);
            self.set_standby();
        }
    }

    fn run_async(
        state: Arc<Mutex<TricoState>>, fname: String, telegram: Notifier,
        email: Notifier, tencent: Notifier
    ) {
        let _th_tokio = std::thread::spawn(move || {
            // our cpu is so weak that any blocking will block our runtime thread.
            // let rt = runtime::Builder::new_current_thread()
            //     .enable_all().build().expect("fail to create runtime");
            // let rt = runtime::Builder::new_multi_thread()
            //     .enable_all().build().expect("fail to create runtime");

            let mut rt = runtime::Runtime::new().expect("fail to create runtime");
            // let _guard = rt.enter();

            rt.spawn(run_config_update(state.clone(), fname));
            rt.spawn(async move { email.run_email().await });
            rt.spawn(async move { tencent.run_tencent().await });
            rt.block_on(async move { telegram.run_telegram().await });
        });
    }

    pub fn run(fname: &str) -> Result<(), Box<dyn std::error::Error>> {
        let state = Arc::new(Mutex::new(TricoState::from_file(fname)?));

        let (tx, rx) = channel::bounded(1024);

        let tx_logtail = tx.clone();
        let mut trico_log = TricoLog::from_state(state.clone());
        let _th_logtail = std::thread::spawn(move || {
            trico_log.run(tx_logtail).unwrap()
        });


        let (telegram, tx_telegram) = Notifier::new(state.clone());
        let (email, tx_email) = Notifier::new(state.clone());
        let (tencent, tx_tencent) = Notifier::new(state.clone());

        Self::run_async(state.clone(), fname.to_string(), telegram, email, tencent);

        {
            debug!("trico started!");
            state.lock().unwrap()
                .bind_notify(tx_email.clone(), true)
                .bind_notify(tx_telegram.clone(), false)
                .bind_notify(tx_tencent.clone(), false)
                .notify("trico started!");
        }

        let alter_timer = channel::tick(std::time::Duration::from_secs(3));
        let mut last_record = Record::info("");
        let mut last_alter = std::time::SystemTime::now() - std::time::Duration::from_secs(60*60*24);
        let mut last_standby = std::time::SystemTime::now();
        loop {
            select! {
                recv(rx) -> record => {
                    if let Ok(record) = record {
                        tx_telegram.send(record.clone()).ok();
                        if record.is_alter { last_record = record; }
                    }
                }
                recv(alter_timer) -> _ => {
                    if last_record.is_alter {
                        if last_alter.elapsed().unwrap() > std::time::Duration::from_secs(60*3) {
                            let state = &mut *state.lock().unwrap();
                            if state.prepare_alter() {
                                tx_email.send(last_record.clone()).ok();
                                tx_tencent.send(last_record.clone()).ok();
                                state.finish_alter();
                            } else {
                                last_record.is_alter = false;
                            }
                            last_alter = std::time::SystemTime::now();
                        }
                    }

                    if last_standby.elapsed().unwrap() > std::time::Duration::from_secs(60) {
                        let state = &mut *state.lock().unwrap();
                        if state.log_alter_standby {
                            state.prepare_alter();
                        }
                        last_standby = std::time::SystemTime::now();
                    }
                }
            }
        }

        // rt.shutdown_timeout(std::time::Duration::from_secs(1));
        // Ok(())
    }
}


impl TricoLog {
    pub fn from_state(state: Arc<Mutex<TricoState>>) -> Self {
        let trico_config;
        {
            let state = state.lock().unwrap();
            trico_config = state.trico_config.clone();
        }

        Self {
            trico_config, state,
            connections:  Default::default(),
            tails:        Default::default(),
        }
    }

    pub fn run(&mut self, tx: Sender<Record>) -> Result<(), Box<dyn std::error::Error>> {
        self.init_ts()?;
        let mut last_fs = HashSet::new();
        loop {
            self.check_config_update();
            self.pool_tails(&tx, &mut last_fs);
            std::thread::sleep(std::time::Duration::from_secs(60));
        }
    }

    fn check_config_update(&mut self) {
        let nconfig = {
            let nconfig = &self.state.lock().unwrap().trico_config;
            if nconfig == &self.trico_config {
                return
            } else {
                nconfig.clone()
            }
        };
        let oconfig = std::mem::replace(&mut self.trico_config, nconfig);
        let nconfig = &self.trico_config;

        // check connections
        let (added, updated_u, removed) = map_diff(oconfig.ssh.keys().collect(), nconfig.ssh.keys().collect());
        let mut updated = HashSet::new();
        for nm in updated_u.iter() {
            let o = oconfig.ssh.get(*nm).unwrap();
            let n = nconfig.ssh.get(*nm).unwrap();
            if o != n {
                updated.insert(*nm);
                self.connections.remove(*nm);
            }
        }
        for nm in removed.iter() { self.connections.remove(*nm); }
        if updated.len() > 0 || removed.len() > 0 || added.len() > 0 {
            info!("ssh section of config changed, added: {:?}, updated: {:?}, removed: {:?}", added, updated, removed);
        }

        // check tails
        let (added, updated_u, removed) = map_diff(oconfig.tail.keys().collect(), nconfig.tail.keys().collect());
        let mut updated = HashSet::new();
        for nm in updated_u.iter() {
            let tconfig = nconfig.tail.get(*nm).unwrap();
            let _config = oconfig.tail.get(*nm).unwrap();
            if tconfig.source == _config.source && tconfig.path == _config.path {
                self.tails.get_mut(*nm).unwrap().update(tconfig);
            } else if tconfig != _config {
                updated.insert(*nm);
                self.tails.insert(nm.to_string(), FileSpec::from_config(tconfig));
            }
        }
        for nm in added.iter() {
            let tconfig = nconfig.tail.get(*nm).unwrap();
            self.tails.insert(nm.to_string(), FileSpec::from_config(tconfig));
        }
        for nm in removed.iter() {
            self.tails.remove(*nm).unwrap();
        }
        if updated.len() > 0 || removed.len() > 0 || added.len() > 0 {
            info!("tails section of config changed, added: {:?}, updated: {:?}, removed: {:?}",
                added, updated, removed);
        }
    }

    fn init_ts(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        for (nm, conf) in self.trico_config.tail.iter() {
            if !self.tails.contains_key(nm) {
                self.tails.insert(nm.to_string(), FileSpec::from_config(conf));
            } else {
                unreachable!()
            }
        }
        self.connections.insert("-".into(), vec![Connection::Local]);
        for (nm, _) in self.trico_config.ssh.iter() {
            if !self.connections.contains_key(nm) {
                self.connections.insert(nm.to_string(), vec![]);
            } else {
                unreachable!()
            }
        }
        Ok(())
    }

    fn notify_fs_change(&self, last_fs: &mut HashSet<String>, this_fs: HashSet<String>) {
        let (added, _, removed) = map_diff(last_fs.iter().collect(), this_fs.iter().collect());
        if removed.len() > 0 || added.len() > 0 {
            let state = &* self.state.lock().unwrap();
            let msg = format!(
                "fs check list changed, added: {:?}, removed: {:?}",
                itertools::join(&added, ", "),
                itertools::join(&removed, ", ")
            );
            info!("{}", msg); state.notify(msg);
        }
        *last_fs = this_fs;
    }

    fn pool_tails(&mut self, tx: &Sender<Record>, last_fs: &mut HashSet<String>) {
        let mut jobs_and_count = Vec::with_capacity(self.trico_config.tail.len());
        let tails_ptr = &mut self.tails as *mut HashMap<String, FileSpec>;
        let mut connection_count = HashMap::new();
        let mut this_fs = HashSet::new();
        for (nm, conf) in self.trico_config.tail.iter() {
            // safety: self.tails is key-to-key map to self.trico_config.tail.
            // And each one don't have ref to one another. So we break it as `&mut key/value` into jobs
            // is ok.
            let fs = unsafe { (&mut *tails_ptr).get_mut(nm).unwrap() };

            if fs.is_checkable() {
                this_fs.insert(nm.to_string());
                if conf.source == "-" {
                    jobs_and_count.push((fs, (conf.source.to_string(), nm.to_string(), 0)));
                } else {
                    let count = connection_count.entry(conf.source.clone()).or_insert(0isize);
                    let conn_conf = self.trico_config.ssh.get(&conf.source).unwrap();
                    let idx = *count % conn_conf.max_connection.max(1).min(10) as isize;

                    jobs_and_count.push((fs, (conf.source.to_string(), nm.to_string(), idx)));
                    *count += 1;
                }
            }
        }

        self.notify_fs_change(last_fs, this_fs);

        for (conn_nm, max_count) in connection_count.into_iter() {
            let conn_conf = match self.trico_config.ssh.get(&conn_nm) {
                Some(c) => c,
                _ => {
                    error!("fail to get connection config for {}", conn_nm);
                    continue;
                }
            };
            let connections = self.connections.entry(conn_nm.to_string()).or_default();
            let used = max_count.max(1).min(conn_conf.max_connection.max(1).min(10) as isize);

            for _ in 0..(used - connections.len() as isize).max(0) {
                match Connection::from_config(conn_conf) {
                    Ok(c) => connections.push(c),
                    Err(e) => {
                        error!("fail to connect {}, {}, {:?}", conn_nm, e, conn_conf);
                    }
                }
            }

            let mut removed = vec![];
            for (idx, conn) in connections.iter_mut().enumerate() {
                if idx < used as _ {
                    conn.refresh();
                } else {
                    if conn.elapsed() > std::time::Duration::from_secs(60*20) {
                        removed.push(idx);
                    }
                }
            }
            if removed.len() > 0 {
                info!("time out removed connections for {}: {}/{}", conn_nm, removed.len(), connections.len());
                for (count, idx) in removed.iter().enumerate() {
                    connections.remove(*idx-count);
                }
            }
        }

        let mut jobs = Vec::with_capacity(jobs_and_count.len());
        for (fs, (source, pretty_name, idx)) in jobs_and_count.into_iter() {
            if let Some(connections) = self.connections.get(&source) {
                if (idx as usize) < connections.len() {
                    jobs.push((fs, connections[idx as usize].mk_ref(), source, pretty_name));
                }
            }
        }

        jobs.into_par_iter().map(|(fs, des, source, pretty_name)| match des {
            ConnectionRef::Local   => {
                fs.pool_local(&source, &pretty_name)
                    .unwrap_or_else(|e| vec![Record::info(format!("{}@{} fail to pool: {}", source, pretty_name, e))])
            },
            ConnectionRef::Sftp(s) => {
                fs.pool_sftp(s, &source, &pretty_name)
                    .unwrap_or_else(|e| vec![Record::info(format!("{}@{} fail to pool: {}", source, pretty_name, e))])
            }})
        .flatten().for_each(
            |record| { tx.send(record).unwrap(); }
        );
    }
}

impl Connection {
    pub fn refresh(&mut self) {
        match self {
            Connection::SSH { ref mut last_refresh, ref mut session, .. } => {
                *last_refresh = std::time::Instant::now();
                debug!("send keepalive {:?}", session.keepalive_send().ok());
            },
            _ => {  }
        }
    }

    pub fn elapsed(&self) -> std::time::Duration {
        match self {
            Connection::Local => std::time::Duration::from_secs(0),
            Connection::SSH { ref last_refresh, .. } => last_refresh.elapsed()
        }
    }

    pub fn from_config(config: &SSHConfig) -> std::io::Result<Self> {
        let name = format!("{}@{}", config.username, config.address);
        info!("mk connect session {}...", name);
        let tcp = std::net::TcpStream::connect(&config.address)?;
        let mut session = ssh2::Session::new().unwrap();

        session.set_tcp_stream(tcp);
        debug!("{} handshake...", name);
        session.handshake()?;

        if !session.authenticated() {
            debug!("{} connect by useragent ...", name);
            let _ = session.userauth_agent(&config.username);
        }
        if !session.authenticated() && config.password.len() > 0 {
            debug!("{} connect by password ...", name);
            let _ = session.userauth_password(&config.username, &config.password);
        }
        if !session.authenticated() && config.privatekey.len() > 0 {
            debug!("{} connect by key ...", name);
            let _ = session.userauth_pubkey_file(
                &config.username,
                config.pubkey.as_ref().map(AsRef::as_ref),
                config.privatekey.as_ref(),
                config.passphrase.as_ref().map(AsRef::as_ref),
            );
        }
        if !session.authenticated() {
            error!("{} authenticate failed! {}@{}", name, config.username, config.address);
        } else {
            info!("{} authenticate success...", name);
        }

        let sftp = session.sftp()?;
        Ok(Self::SSH { session, sftp, last_refresh: std::time::Instant::now() })
    }

    pub fn mk_ref(&self) -> ConnectionRef {
        match &self {
            Connection::SSH { sftp, .. } => ConnectionRef::Sftp(sftp),
            Connection::Local            => ConnectionRef::Local
        }
    }
}

