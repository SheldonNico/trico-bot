// use ring::digest::{Context, SHA256};
// use data_encoding::HEXLOWER;
use std::fs;
use std::error::Error;
use std::io::{self, Read};
use std::path::Path;
use chrono::{DateTime, Local};
use std::collections::HashSet;
use std::hash::Hash;
use sha2::{Sha256, Sha512, Digest};

pub fn modified_time(filepath: &Path) -> Result<DateTime<Local>, Box<dyn Error>> {
    let metadata = fs::metadata(&filepath)?;
    Ok(metadata.modified()?.into())
}

// add, update, removed
pub fn map_diff<'a, V: Eq+Hash>(ori: HashSet<&'a V>, new: HashSet<&'a V>) -> (HashSet<&'a V>, HashSet<&'a V>, HashSet<&'a V>) {
    (
        new.difference(&ori).map(|s| *s).collect(),
        new.intersection(&ori).map(|s| *s).collect(),
        ori.difference(&new).map(|s| *s).collect()
    )
}

// pub fn sha256_digest<R: Read>(mut reader: R) -> io::Result<String> {
//     let mut context = Context::new(&SHA256);
//     let mut buffer = [0; 1024];

//     loop {
//         let count = reader.read(&mut buffer)?;
//         if count == 0 {
//             break;
//         }
//         context.update(&buffer[..count]);
//     }

//     Ok(HEXLOWER.encode(context.finish().as_ref()))
// }


pub fn sha256_digest<R: Read>(mut reader: R) -> io::Result<String> {
    let mut hasher = Sha256::new();
    let mut buffer = [0; 1024];

    loop {
        let count = reader.read(&mut buffer)?;
        if count == 0 {
            break;
        }
        hasher.update(&buffer[..count]);
    }

    Ok(format!("{:x}", hasher.finalize()))
}
