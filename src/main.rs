use dotenv::dotenv;
use log::*;

fn main() {
    dotenv().ok();
    env_logger::init();

    let fname = std::env::var("TRICO_CONFIG").unwrap_or("config.toml".into());
    info!("load config from: {}", fname);
    trico_bot::trico::TricoState::run(&fname).unwrap();
}
