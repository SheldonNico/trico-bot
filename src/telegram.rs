use crate::trico::TricoState;

use futures::StreamExt;
use telegram_bot::{*, Error as TelegramError};
use std::sync::{Arc, Mutex};
use log::*;
use lazy_static::lazy_static;

const INLINE_KEYBOARD_LEN: usize = 30;
lazy_static! {
    static ref UPLOAD_FOLDER: std::path::PathBuf = {
        let folder = std::env::var("TRICO_UPLOAD_FOLDER").unwrap_or("./assets".into());
        folder.into()
    };
}

type BoxError = Box<dyn Send+Sync+std::error::Error>;
type ArcState = Arc<Mutex<TricoState>>;

pub async fn start_bot(state: ArcState, telegram: Api, chat: Chat) {
    let mut stream = telegram.stream();
    info!("telegram starting ...");
    while let Some(update) = stream.next().await {
        debug!("got update from telegram: {:?}", update);
        match update {
            Err(e) => {
                error!("Fail to read update: {:?}", e);
            }
            Ok(update) => match update.kind {
                UpdateKind::Message(message) => {
                    let _ = tokio::spawn(handle_msg(telegram.clone(), chat.clone(), state.clone(), message));
                }
                UpdateKind::CallbackQuery(callbackquery) => {
                    let _ =
                        tokio::spawn(handle_callback(telegram.clone(), chat.clone(),state.clone(), callbackquery));
                }
                _ => {
                    info!("Got update {:?}...", update);
                }
            },
        }
    }
}

// monitors - list all monitors
// standy - switch into standy state
// continue - cancel standby state
// help - get help
pub async fn handle_msg(telegram: Api, chat: Chat, state: ArcState, message: Message) -> Result<(), BoxError> {
    if message.chat.to_chat_ref() != chat.to_chat_ref() {
        telegram.send(message.text_reply(
                "\
                     Sorry this bot is for private usage. \
                     You do not have access to it. \
                     Please contact with the bot admin to get access.",
        )).await?;
        return Ok(());
    }

    match message.kind {
        MessageKind::Text { ref data, .. } => match data.as_str() {
            "/monitors" => reply_monitors(telegram, state, &message, false).await?,
            "/help"     => reply_help(telegram, state, &message, false).await?,
            "/standby"  => reply_monitors_alter(telegram, state, "standby", &message, false).await?,
            "/continue" => reply_monitors_alter(telegram, state, "continue", &message, false).await?,
            //"/exec" => (),
            _ => (),
        },
        MessageKind::Document { ref data, .. } => {
            let cap = data.file_name.clone();
            if cap.is_some() {
                let cap = cap.unwrap();
                let _fh = std::fs::File::create(&UPLOAD_FOLDER.join(cap))
                    .expect("Fail to create upload folder");
                println!("{:?}", data.to_file_ref());
                //fh.write(data);
            }
        }
        _ => { debug!("got message: {:?}", &message); }
    }
    Ok(())
}

pub async fn handle_callback(telegram: Api, chat: Chat, state: ArcState, callbackquery: CallbackQuery) -> Result<(), BoxError> {
    if let Some(MessageOrChannelPost::Message(ref message)) = callbackquery.message {
        if message.to_source_chat() != chat.id() {
            telegram.send(message.text_reply(
                    "\
                     Sorry this bot is for private usage. \
                     You do not have access to it. \
                     Please contact with the bot admin to get access.",
            )).await?;
            return Ok(());
        }

        match callbackquery.data.as_ref().unwrap().as_str() {
            "monitors"          => reply_monitors(telegram, state, &message, true).await?,
            "monitors.disable"  => reply_monitors_cmd(telegram, state, &message, "disable", true).await?,
            "monitors.enable"   => reply_monitors_cmd(telegram, state, &message, "enable", true).await?,
            "monitors.continue" => reply_monitors_alter(telegram, state, "continue", &message, true).await?,
            "monitors.standby"  => reply_monitors_alter(telegram, state, "standby", &message, true).await?,
            "exit"              => reply_exit(telegram, state, &message, true).await?,
            "exec"              => reply_exit(telegram, state, &message, true).await?,
            cmd if cmd.starts_with("monitors.disable.") => {
                reply_monitors_switch(telegram, state, message, &cmd[9..], true).await?
            }
            cmd if cmd.starts_with("monitors.enable.") => {
                reply_monitors_switch(telegram, state, message, &cmd[9..], true).await?
            }
            _ => {
                debug!("Got callback query can't handle: {:?}", callbackquery);
            }
        }
    } else {
        debug!("Got callback query from invalid message: {:?}.", callbackquery);
    }

    Ok(())
}

async fn reply_monitors(telegram: Api, state: ArcState, message: &Message, is_callback: bool) -> Result<(), BoxError> {
    let keyboard = vec![
        ("disable", "monitors.disable"),
        ("enable", "monitors.enable"),
        ("standby", "monitors.standby"),
        ("continue", "monitors.continue"),
    ];
    let markup = arrange_keyboard(keyboard);

    let mut msg = "Monitors informations".to_string();
    {
        let state = & *state.lock().unwrap();
        let mut unused = "".to_string();

        for (tname, tconfig) in state.trico_config.tail.iter() {
            if tconfig.is_disable {
                unused.push_str(&format!(",{}", tname));
            } else {
                msg.push_str(&format!("\n`{}@{}: {}`", tname, tconfig.source, tconfig.path));
            };
        }

        if unused.len() > 1 {
            msg.push_str(&format!("\n\n`Disabled: {}`", &unused[1..]));
        }
    }

    if is_callback {
        telegram.send(
            message.edit_text(msg).parse_mode(ParseMode::Markdown).reply_markup(markup)
        ).await?;
    } else {
        telegram.send(
            message.chat.text(msg).parse_mode(ParseMode::Markdown).reply_markup(markup)
        ).await?;
    }

    Ok(())
}

async fn reply_monitors_cmd(telegram: Api, state: ArcState, message: &Message, cmd: &str, _is_callback: bool) -> Result<(), BoxError> {
    let cmd_enable = match cmd {
        "enable" => true,
        "disable" => false,
        _ => unreachable!(),
    };

    let mut watcherlist = vec![];
    {
        let state = &*state.lock().unwrap();

        for (tname, tconfig) in state.trico_config.tail.iter() {
            if tconfig.is_disable == cmd_enable {
                watcherlist.push((tname.to_string(), format!("monitors.{}.{}", cmd, tname)));
            }
        }
    }

    trace!("Answer monitors.disable/enable");
    if watcherlist.len() == 0 {
        let mut reply = message.edit_text(format!("No watcher available to {}: ", cmd));
        reply.parse_mode(ParseMode::Markdown);
        telegram.send(reply).await?;
    } else {
        let keyboard: Vec<(&str, &str)> = watcherlist
            .iter()
            .map(|(s1, s2)| (s1.as_ref(), s2.as_ref()))
            .collect();
        let markup = arrange_keyboard(keyboard);

        let mut reply =
            message.edit_text(format!("Here is your watch list, choose one to {}: ", cmd));
        reply.parse_mode(ParseMode::Markdown).reply_markup(markup);
        telegram.send(reply).await?;
    }

    Ok(())
}

async fn reply_monitors_switch(telegram: Api, state: ArcState, message: &Message, cmd: &str, _is_callback: bool)
    -> Result<(), BoxError>
{
    let (cmd, watchername) = match cmd {
        cmd if cmd.starts_with("disable.") => ("disable", &cmd[8..]),
        cmd if cmd.starts_with("enable.")  => ("enable", &cmd[7..]),
        _ => { return Ok(()) }
    };

    let mut msglist = vec![];
    {
        let state = &mut *state.lock().unwrap();

        for (tname, tconfig) in state.trico_config.tail.iter_mut() {
            if tname == watchername {
                if tconfig.is_disable && cmd == "enable" {
                    tconfig.is_disable = false;
                    msglist.push(format!("{}@{} change to {}", tname, tconfig.source, cmd));
                } else if !tconfig.is_disable && cmd == "disable" {
                    tconfig.is_disable = true;
                    msglist.push(format!("{}@{} change to {}", tname, tconfig.source, cmd));
                } else {
                    msglist.push(format!("{}@{} already {}, unchanbed!", tname, tconfig.source, cmd));
                }
            }
        }
    }

    let msg = if msglist.len() > 0 {
        msglist.join(",")
    } else {
        "watcher not inside config, this can happen if your message is too old.".to_string()
    };

    trace!("Reply to monitors.{} {} => {}", cmd, watchername, msg);
    telegram.send(message.edit_text(msg)).await?;
    Ok(())
}

async fn reply_exit(telegram: Api, _state: ArcState, message: &Message, _is_callback: bool) -> Result<(), TelegramError> {
    telegram.send(message.delete()).await?;
    Ok(())
}

async fn reply_help(telegram: Api, _state: ArcState, message: &Message, is_callback: bool) -> Result<(), TelegramError> {
    let markup = reply_markup!(inline_keyboard,
        ["monitors" callback "monitors", "exec" callback "exec"]
    );
    let msg = "Trico bot:\n\
               simple bot help you _monitor log_, _execute cmd_.\
               ";

    if is_callback {
        telegram.send(
            message.edit_text(msg).parse_mode(ParseMode::Markdown).reply_markup(markup)
        ).await?;
    } else {
        telegram.send(
            message.chat.text(msg).parse_mode(ParseMode::Markdown).reply_markup(markup)
        ).await?;
    }


    Ok(())
}

async fn reply_monitors_alter(telegram: Api, state: ArcState, cmd: &str, message: &Message, is_callback: bool) -> Result<(), BoxError> {
    let cmd_standby = match cmd {
        "standby" => true,
        "continue" => false,
        _ => return Ok(()),
    };

    let msg;
    {
        let state = &mut *state.lock().unwrap();
        match (state.log_alter_standby, cmd_standby) {
            (true, true) => {
                // state.log_alter_last = std::time::Instant::now();
                msg = "standby already set to be true";
            }
            (false, false) => {
                msg = "standby already set to be false";
            }
            (true, false) => {
                msg = "change to continue";
                state.reset_standby();
            }
            (false, true) => {
                // state.log_alter_last = std::time::Instant::now();
                msg = "change to standby";
                state.set_standby();
            }
        }
    }

    info!("{} from telegram", msg);
    if is_callback {
        telegram.send(message.edit_text(msg)).await?;
    } else {
        telegram.send(message.chat.text(msg)).await?;
    }

    Ok(())
}

fn arrange_keyboard(mut keyboard: Vec<(&str, &str)>) -> InlineKeyboardMarkup {
    let mut markup = InlineKeyboardMarkup::new();
    keyboard.push(("<<exit", "exit"));

    let mut rows = vec![];
    let mut last_row_len = 0;
    for (msg, cmd) in keyboard.into_iter() {
        if last_row_len + msg.len() < INLINE_KEYBOARD_LEN {
            last_row_len += msg.len();
            rows.push(InlineKeyboardButton::callback(msg, cmd));
        } else {
            markup.add_row(std::mem::replace(&mut rows, vec![]));
            last_row_len = msg.len();
            rows.push(InlineKeyboardButton::callback(msg, cmd));
        }
    }
    markup.add_row(rows);

    markup
}
