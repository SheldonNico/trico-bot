use crate::configs::*;

use chrono::{DateTime, Local, TimeZone, Weekday};
use ssh2::{Session, Sftp};
use rayon::prelude::*;
use regex::Regex;
use log::*;
use serde::{self, de::{self, IntoDeserializer, Visitor, Deserializer}, Serialize, Deserialize};

use std::collections::{HashMap, HashSet};
use std::path::{PathBuf, Path};
use std::io::{self, Read, BufRead, BufReader, SeekFrom, Seek};

const MAX_SEEK: u64 = 1024 * 1024;
const MAX_LOG_NUM: usize = 30;

const LF_BYTE: u8 = '\n' as u8;
const CR_BYTE: u8 = '\r' as u8;

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum FileSpecStatus {
    Uninit,  // not started
    Working, // started and working...
}

/// source:
///     "": system
///     "-": local
///     "xxx": remote
#[derive(Clone, Debug)]
pub struct Record {
    pub source: String,
    pub msg: String,
    pub timestamp: String,
    pub level: log::Level,
    pub is_alter: bool,
}

impl Record {
    pub fn info<S: Into<String>>(msg: S) -> Self {
        Self {
            source: "".into(),
            msg: msg.into(),
            timestamp: "".into(),
            level: log::Level::Info,
            is_alter: false,
        }
    }

    pub fn alter<S: Into<String>>(msg: S) -> Self {
        Self {
            source: "".into(),
            msg: msg.into(),
            timestamp: "".into(),
            level: log::Level::Warn,
            is_alter: true,
        }
    }
}

/// Options:
///
/// 1. is_follow: keep last no-empty line in cache and print it out when next line arrived
/// 2. offset: read last n(offset_n) line, negative means no offfset
#[derive(Debug, Clone)]
pub struct FileSpec {
    pub path: String,
    pub ignored_regex: Vec<Regex>,
    pub time_span: Vec<TimeSpan>,
    pub outdated_min: i32,
    pub offset_n: i32,

    pub is_disable: bool,
    pub is_not_found_alter: bool,
    pub is_follow: bool,

    pub log_regex: Option<Regex>,
    pub log_level_map: HashMap<String, String>,
    pub log_level_alter: log::Level,
    pub log_level_notice: log::Level,

    // state
    path_noregex: PathBuf,
    curr_seek: u64,
    basename: String,
    status: FileSpecStatus,
    last_ts_checked: DateTime<Local>,
    last_ts_pooled: DateTime<Local>,
}

impl FileSpec {
    pub fn from_config(config: &TailConfig) -> Self {
        FileSpec {
            path: config.path.clone(),
            ignored_regex: config.ignored_regex.iter().map(|r| r.0.clone()).collect(),
            time_span: config.time_span.clone(),
            outdated_min: config.outdated_min,
            offset_n: config.offset_n,

            is_disable: config.is_disable,
            is_not_found_alter: config.is_not_found_alter,
            is_follow: config.is_follow,

            log_regex: config.log_regex.clone().map(|r| r.0.clone()),
            log_level_map: config.log_level_map.iter().map(|(k, r)| (k.clone(), r.clone())).collect(),
            log_level_alter: config.log_level_alter,
            log_level_notice: config.log_level_notice,

            // state
            path_noregex: PathBuf::new(),
            curr_seek: 0,
            basename: String::new(),
            status: FileSpecStatus::Uninit,
            last_ts_checked: Local::now() - chrono::Duration::days(7),
            last_ts_pooled: Local::now() - chrono::Duration::days(7),
        }
    }

    pub fn update(&mut self, config: &TailConfig) {
        self.path = config.path.clone();
        self.ignored_regex = config.ignored_regex.iter().map(|r| r.0.clone()).collect();
        self.time_span = config.time_span.clone();
        self.outdated_min = config.outdated_min;
        self.offset_n = config.offset_n;
        self.is_disable = config.is_disable;
        self.is_not_found_alter = config.is_not_found_alter;
        self.is_follow = config.is_follow;
        self.log_regex = config.log_regex.clone().map(|r| r.0.clone());
        self.log_level_map = config.log_level_map.iter().map(|(k, r)| (k.clone(), r.clone())).collect();
        self.log_level_alter = config.log_level_alter;
        self.log_level_notice = config.log_level_notice;
    }

    /// open api: for outside to decide whether to run pool.
    ///
    /// - !is_follow: pool invoked only once when during self.last_ts
    /// - is_follow: pool invoked when during self.last_ts
    pub fn is_checkable(&self) -> bool {
        if self.is_disable { return false; }
        if let Some(ts) = self.check_time(chrono::Local::now()) {
            // if ts is still last ts, self.last_ts_checked and self.last_ts_pooled must sit in the
            // same ts range.
            if self.status != FileSpecStatus::Uninit && ts.is_in_span(&self.last_ts_checked) && ts.is_in_span(&self.last_ts_pooled) {
                if self.is_follow {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
        false
    }

    pub fn pool_local(&mut self, source: &str, pretty_name: &str) -> Result<Vec<Record>, Box<dyn std::error::Error>> {
        self.last_ts_pooled = chrono::Local::now();
        let mut out = vec![];
        // check is time suitable
        if !self.is_checkable() { return Ok(out); }
        let path_with_dt = Self::update_path_by_date(&self.path, &chrono::Local::now());
        debug!("start search in path: {}", path_with_dt);

        // check is file right: available + last modified time right.
        match self.locate_fname_local(path_with_dt.as_ref()) {
            Err(e) if e.kind() == io::ErrorKind::NotFound => {
                if self.is_not_found_alter { error!("{} fail to found file", e); }
                return Ok(out)
            },
            Ok(delta) => {
                if self.outdated_min >= 0 && delta / 60 > self.outdated_min as u64 {
                    // destination file stop refresh for self.outdated_min
                    let msg = format!(
                        "{}@{}: {} too old, last modified: {}", pretty_name, source, self.path_noregex.display(),
                        (Local::now()-chrono::Duration::seconds(delta as _)).format("%Y-%m-%d %H:%M:%S")
                    );
                    error!("{}", msg);
                    out.push(Record::alter(msg));
                }
            }
            Err(e) => {
                return Err(Box::new(e));
            }
        }
        debug!("start parse content in path: {}", self.path_noregex.display());

        let mut buf = BufReader::new(std::fs::File::open(&self.path_noregex)?);
        // check file content
        self.tail(&mut buf, source, pretty_name, &mut out)?;
        self.last_ts_checked = chrono::Local::now();
        debug!("finished parse content for {}", self.path_noregex.display());
        Ok(out)
    }

    pub fn pool_sftp(&mut self, sftp: &Sftp, source: &str, pretty_name: &str)
        -> Result<Vec<Record>, Box<dyn std::error::Error>>
    {
        self.last_ts_pooled = chrono::Local::now();
        let mut out = vec![];
        if !self.is_checkable() { return Ok(out); }
        let path_with_dt = Self::update_path_by_date(&self.path, &chrono::Local::now());
        debug!("start search in path: {}", path_with_dt);

        match self.locate_fname_sftp(path_with_dt.as_ref(), sftp) {
            Err(e) if e.code() == -31 => {
                if self.is_not_found_alter { error!("fail to found file: {}", e); }
                return Ok(out)
            },
            Ok(delta) => {
                if self.outdated_min >= 0 && delta / 60 > self.outdated_min as u64 {
                    // destination file stop refresh for self.outdated_min
                    let msg = format!(
                        "{}@{}: {} too old, last modified: {}", pretty_name, source, self.path_noregex.display(),
                        (Local::now()-chrono::Duration::seconds(delta as _)).format("%Y-%m-%d %H:%M:%S")
                    );
                    error!("{}", msg);
                    out.push(Record::alter(msg));
                }
            }
            Err(e) => {
                return Err(Box::new(e));
            }
        }
        debug!("start parse content in path: {}", self.path_noregex.display());

        let mut buf = BufReader::new(sftp.open(self.path_noregex.as_ref())?);
        self.tail(&mut buf, source, pretty_name, &mut out)?;
        self.last_ts_checked = chrono::Local::now();
        trace!("finished parse content for {}", self.path_noregex.display());
        Ok(out)
    }

    fn locate_fname_sftp(&mut self, des: &Path, sftp: &Sftp) -> Result<u64, ssh2::Error> {
        let stat = sftp.stat(&des)?;

        if stat.is_dir() {
            self.path_noregex = sftp
                .readdir(&des)?
                .into_iter()
                .filter(|(_, st)| st.is_file() && st.mtime.is_some())
                .max_by_key(|(_, st)| st.mtime.unwrap())
                .ok_or(ssh2::Error::unknown())?.0;
        } else {
            self.path_noregex = des.to_path_buf();
        }

        let stat = sftp.stat(&self.path_noregex)?;
        if stat.is_file() {
            let stime = stat.mtime.ok_or(ssh2::Error::unknown())?;
            let delta = std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap().as_secs() - stime;
            self.basename = self.path_noregex.file_name().unwrap_or_default().to_string_lossy().into();
            Ok(delta)
        } else {
            Err(ssh2::Error::from_errno(-31))
        }
    }

    fn locate_fname_local(&mut self, des: &Path) -> io::Result<u64> {
        if des.is_dir() {
            self.path_noregex = std::fs::read_dir(&des)?
                .into_iter()
                .filter_map(|de| de.ok())
                .filter(|de| de.metadata().is_ok() && de.metadata().unwrap().is_file())
                .max_by_key(|de| de.metadata().unwrap().modified().unwrap())
                .unwrap().path();
        } else {
            self.path_noregex = des.to_path_buf();
        }

        let stime = std::fs::metadata(&self.path_noregex)?.modified()?;
        let delta = std::time::SystemTime::now().duration_since(stime).unwrap().as_secs();
        self.basename = self.path_noregex.file_name().unwrap_or_default().to_string_lossy().into();
        Ok(delta)
    }

    fn check_time<T: chrono::Timelike + chrono::Datelike>(&self, dt: T) -> Option<TimeSpan> {
        for ts in self.time_span.iter() {
            if ts.is_in_span(&dt) {
                return Some(ts.clone());
            }
        }
        None
    }

    /// use loop to read backwards, but seek multiple times badly.
    fn adjust_seek_start<R: Seek+Read> (&mut self, buf: &mut R) -> Result<(), Box<dyn std::error::Error>> {
        let LEN: i64 = 1024;
        let mut left = buf.seek(SeekFrom::End(0))? as i64;

        let mut content = vec![0; LEN as usize];
        let mut count = 0;
        // if self.is_follow, add one line to make sure print n line and keep one line in cache
        let offset_n = if self.is_follow { self.offset_n + 1 } else { self.offset_n };

        'outer: while left > 0 {
            let step = left.min(LEN);

            left -= step;
            buf.seek(SeekFrom::Current(-step))?;
            buf.read_exact(&mut content[0..(step as usize)])?;
            buf.seek(SeekFrom::Current(-step))?;

            for (idx, b) in content[0..(step as usize)].iter().rev().enumerate() {
                if *b == LF_BYTE {
                    count += 1;
                    // \n -- \n -- \n
                    // two line should pass 3 newline
                    if count > offset_n {
                        self.curr_seek = buf.seek(SeekFrom::Current(step-idx as i64))?;
                        break 'outer;
                    }
                }
            }
        }

        Ok(())
    }

    fn adjust_seek<R: Seek> (&mut self, buf: &mut R) -> Result<(), Box<dyn std::error::Error>> {
        let max_len = buf.seek(SeekFrom::End(0))?;
        if max_len > self.curr_seek && max_len - self.curr_seek > MAX_SEEK {
            // debug!("{} seek point far from end {} -> {}, re select seek point", self.name, self.curr_seek, max_len);
            if max_len > MAX_SEEK {
                self.curr_seek = buf.seek(SeekFrom::Start(max_len-MAX_SEEK))?;
            } else {
                self.curr_seek = buf.seek(SeekFrom::Start(0))?;
            }
        } else {
            self.curr_seek = buf.seek(SeekFrom::Start(self.curr_seek))?;
        }

        Ok(())
    }

    fn update_path_by_date(path: &str, now: &DateTime<Local>) -> String {
        let is_format_now = regex::Regex::new(r"\{today:(.*)\}").unwrap();
        let mut replaced = String::new();
        let mut lastst = 0;
        if is_format_now.find(path).is_some() {
            for cap in is_format_now.captures_iter(path) {
                let st = cap.get(0).unwrap().start();
                let ed = cap.get(0).unwrap().end();
                replaced.push_str(&path[lastst..st]);
                lastst = ed;

                let mat = cap.get(1).unwrap();
                replaced.push_str(&now.format(mat.as_str()).to_string());
            }
        }
        replaced.push_str(&path[lastst..]);
        replaced
    }

    /// track last line. Because our program works on multiple files, we need keep tracking
    /// last_line break point.
    fn tail<R: BufRead+Seek>(&mut self, buf: &mut R, source: &str, pretty_name: &str, out: &mut Vec<Record>)
        -> Result<(), Box<dyn std::error::Error>>
    {
        if self.status == FileSpecStatus::Uninit && self.offset_n >= 0 {
            self.adjust_seek_start(buf)?;
            self.status = FileSpecStatus::Working;
        } else {
            self.adjust_seek(buf)?;
        }

        let mut msg = String::new(); let mut cached = vec![]; let mut last_len = 0;
        while let Ok(len) = buf.read_line(&mut msg) {
            if len == 0 {
                if self.is_follow { self.curr_seek -= last_len as u64; }
                break;
            } // reach eof
            self.curr_seek += len as u64;
            cached.push(msg.clone());
            if !self.is_follow || cached.len() > 1 {
                let m = cached.remove(0);
                let line = m.trim_end();

                // check ignored list
                let mut is_ignored = false;
                for pattern in self.ignored_regex.iter() {
                    if pattern.find(line).is_some() {
                        is_ignored = true;
                        break;
                    }
                }
                if is_ignored { continue; }

                // check content
                if let Some(ref pattern) = self.log_regex {
                    // check log format
                    if let Some((level, timestamp, msg)) = self.parse_log(pattern, line) {
                        if level <= self.log_level_notice {
                            if level <= self.log_level_alter {
                                warn!("[{}@{} {:<5} {}] {}", pretty_name, source, level, timestamp, msg);
                            } else {
                                info!("[{}@{} {:<5} {}] {}", pretty_name, source, level, timestamp, msg);
                            }

                            out.push(Record {
                                source: format!("{}@{}", pretty_name, source),
                                level: level,
                                timestamp: timestamp.to_string(),
                                msg: msg.to_string(),
                                is_alter: level <= self.log_level_alter,
                            });
                        }
                    }
                } else {
                    // check normal format
                    println!("{}@{}: {}", pretty_name, source, line);
                }
            }
            msg.clear();
            last_len = len;
        }

        Ok(())
    }

    pub fn parse_log<'a>(&self, pattern: &Regex, line: &'a str) -> Option<(log::Level, &'a str, &'a str)> {
        let caps = pattern.captures(line.trim())?;
        let mut level = caps.name("level")?.as_str();
        let timestamp = caps.name("timestamp")?.as_str();
        let msg = caps.name("msg")?.as_str();

        for (expr_ori, expr) in self.log_level_map.iter() {
            if expr_ori == level {
                level = expr;
                break;
            }
        }

        let level: log::Level = level.parse()
            .map_err(|e| { warn!("fail to parse loglevel in line  `{:?}` `{:?}`", level, line); e })
            .ok()?;
        Some((level, timestamp, msg))
    }
}

pub fn demo() {
    use std::net::TcpStream;

    let mut sessions: Vec<Session> = vec![];
    let mut sftps = vec![];
    let mut fs = vec![];

    for _ in 0..3 {
        // let tcp = TcpStream::connect("192.168.1.99:22").unwrap();
        // let mut sess = Session::new().unwrap();
        // sess.set_tcp_stream(tcp);
        // sess.handshake().unwrap();
        // sess.userauth_password("xnie", "SJTU2012@kaina").unwrap();

        // let sftp = sess.sftp().unwrap();
        // sftps.push(sftp);

        sftps.push(());

        fs.push(FileSpec::from_config(
                & TailConfig {
                    path: "/home/xnie/tmp.py".into(),
                    outdated_min: 0,
                    is_follow: false,
                    offset_n: -1,

                    ..Default::default()
                }
        ));
        // fs.push(FileSpec::new("/home/xnie/projects/jobs/opt/logs/ajmd6/20201022/opt.log", false, 10));
    }

/*
 *     let j: Vec<(_, _)> = sftps.iter().zip(fs.iter_mut()).collect();
 *     let _j: Vec<_> = j.into_par_iter()
 *         .map(|(s, f)| f.pool_local().unwrap())
 *         // .map(|(s, f)| f.pool_sftp(s).unwrap())
 *         .collect();
 *
 *     println!(">>>>>>>>>");
 *
 *     let j: Vec<(_, _)> = sftps.iter().zip(fs.iter_mut()).collect();
 *     let _j: Vec<_> = j.into_par_iter()
 *         .map(|(s, f)| f.pool_local().unwrap())
 *         // .map(|(s, f)| f.pool_sftp(s).unwrap())
 *         .collect();
 *     println!("----------");
 */
}

impl Default for FileSpecStatus {
    fn default() -> Self { Self::Uninit }
}

