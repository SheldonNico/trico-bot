# Trico (WIP)
A tiny telegram bot to do some small tasks, (private useage only, which means you should run this bot with your own bot on your own server):

1. monitor: monitor log files. I've use this method to monitor my log file for almost 1 year, it's stable and easy to use.
2. upload file: send any media to the bot, it will store it in a preset folder (default upload). You can then set some automatic task like forward it to your disk or download a torrent.
3. execute preset script: just like risc bot, it can exec some simple script inside a preset folder (default exec).

# TODO
1. make path recognize "~".

# Credits
Great thanks to [risc bot](https://github.com/timvisee/risc-bot), which provide a good framework to write a bot.


